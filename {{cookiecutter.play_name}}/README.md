[![pipeline status](https://git.coop/aptivate/ansible-plays/{{ cookiecutter.play_name }}/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-plays/{{ cookiecutter.play_name }}/commits/master)

# {{ cookiecutter.play_name }}

{{ cookiecutter.description }}

## Get Started

Install the Python dependencies with:

```bash
$ pipenv install --dev --three {{ cookiecutter.python_version }}
```

And then retrieve the Ansible dependencies with:

```bash
$ pipenv run ansible-galaxy install
```

Configure your environment with:

```bash
$ mv .env.example .env
```

You may need to edit this file. It is loaded automatically by Pipenv.
